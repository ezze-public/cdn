
$(function() {
	
	var curr_dropsearch = null;
	var dropsearch_over = false;

	$('[dropsearch]').on('focusin', function(e){
		$(this).select();
	});

	$('[dropsearch]').on('focusin keyup', function(e){
		if( e.keyCode != 27 ){
			open_dropsearch( $(this) );
			curr_dropsearch = $(this);
			dropsearch_over = true;
			// console.log('oppened');
		}
	
	}).on('focusout', function(){
		dropsearch_over = false;
	
	}).on('keydown', function(e){
		if( curr_dropsearch && e.keyCode == 27 ){
			close_dropsearch( $(this) );
			curr_dropsearch = null;
			e.preventDefault();
		} else {
			//
		}
	});

	// close the box, on out click.
	$('body').on('click', function(){
		if(! dropsearch_over ) close_dropsearch( $(this) );
	});

	// select the item
	$('body').delegate('.dropsearch_select > *', 'click', function() {
		dropsearch_select_this_option( $(this) );
	});
	

});




function open_dropsearch( t ){

	close_all_dropsearch();
	
	var w = t.width();
	var h = t.height();
	var p = t.parent();

	p.css({position:'relative'});

	var wp = pxInt(t.css('padding-left')) + pxInt(t.css('padding-right'));
	var hp = pxInt(t.css('padding-top')) + pxInt(t.css('padding-bottom'));

	var feed = t.attr('dropsearch');

	if(! t.parent().find('.dropsearch_select').length ){

		t.addClass('dropsearch_loading');

		var code = '';
		var obj = {};
		obj[ t.attr('name') ] = t.val();
		$.ajax({url:feed,type:'POST',data:obj}).done(function(r){
			if( r['req'] == t.val() ){
				
				$.each(r['res'], function(i, item) {
					// console.log(item);
					code+= '<li>'+item+'</li>';
				});

				t.removeClass('dropsearch_loading');
				
				if( code ){
					t.after('<ul class=dropsearch_select >'+code+'</ul>');
					p.find('.dropsearch_select').css({'width':(w+wp-8)+'px',top: (h+hp)+'px'});
				}

			}
		});

	}

}


function close_dropsearch( t ){
	t.parent().find('.dropsearch_select').remove();
}

function close_all_dropsearch(){
	$('.dropsearch_select').remove();
}



function dropsearch_select_this_option( t ){
	t.parent().parent().find('[dropsearch]').val( t.html().replace('&amp;', '&') );//.focus();
}



function pxInt( px ){
	return parseInt( px.replace('px', ''));
}


