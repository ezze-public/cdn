
// v-1.0


function rand( n ){
	return Math.round( Math.random() * Math.pow( 10 , n ) );
}

function alerty( t, hide_delay=4 ){
jQuery(document).ready(function($) {
	
	if( $('#alerty').length == 0 ){
		$('body').append('<div id="alerty"></div>');
	}
	
	msg_id = 'msg'+rand(6);
	$('#alerty').append('<div class="msg" id="'+msg_id+'" ></div>');
	
	$('#alerty #'+msg_id).hide();
	$('#alerty #'+msg_id).html( t );
	$('#alerty #'+msg_id).show();

	if( hide_delay != 0 ){
		alerty_hide( msg_id, hide_delay );
	}

});
}


function alerty_hide( id, hide_delay ){
	
	setTimeout( function(){
		$('#alerty #'+id).addClass('hide');
	}, hide_delay * 1000 );
	
	setTimeout( function(){
		$('#alerty #'+id).remove();
	}, hide_delay * 1000 + 2000 );

}


var old_alert = alert;
alert = function( t, hide_delay ) {
	alerty( t, hide_delay );
}

