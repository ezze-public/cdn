
// v-1.5.1
// 26 jul 2020

// var active_window;

jQuery(document).ready(function($) {

	// $(window).focus(function() {
	// 	if(! active_window ){
	//     	active_window = 1;
	//     	console.log('window: focus');
	//     }
	// });

	// $(window).blur(function() {
	// 	if( active_window == 1 ){
	// 	    active_window = 0;
	//     	console.log('window: blur');
	// 	}
	// });

	//
	// global - do-ajax
	$('.do-ajax,[do-ajax]').on('click change', function(e){

		// vars
		t = $(this);

		var tagName = t.prop('tagName');
		var eventName = e.handleObj.type;

		// console.log( tagName + ' // ' +  eventName);

		if( (tagName == 'SELECT' && eventName != 'change') || (tagName != 'SELECT' && eventName == 'change') )
			return;

		// ignore interval, if its human
		if( t.attr('interval') )
			// human click, or normal click
			if( e.originalEvent !== undefined )
				return;

		if( t.prop('tagName') == 'INPUT' && t.attr('type') == 'file' )
			return true;

		// disabled semaphore
		if( t.attr('disabled') )
			return;

		// hide=1
		if( t.attr('hide') == 1 )
			if(! t.attr('prompt') && !t.attr('confirm') )
				t.hide();

		// show=.re/input[type=submit]
		if( show = t.attr('show') ){
			show_arr = show.split('/');
			t.closest( show_arr[0] ).find( show_arr[1] ).show();
		}

		// disload=1
		if( t.attr('disload') == 1 ){
			// t.toggleClass('disabled');
			t.attr('disabled', 'disabled');
		}

		// ajax href
		if( t.attr('href') || t.attr('feed') || t.attr('do-ajax') ){

			// confirm
			if( t.attr('confirm') && !confirm('Are you sure?') )
				return;

			// multiple trigger
			if( t.attr('do-ajax') ){
				href = t.attr('do-ajax');
			} else if( t.attr('feed') ){
				href = t.attr('feed');
			} else if( t.attr('href') ){
				href = t.attr('href');
			} else {
				return;
			}

			// da-rel
			// $.getScript( "../jquery.md5/jquery.md5.js" );

			t.attr('da-rel', md5(href) );

			// env
			href+= (href.indexOf('?') == -1 ? '?': '&')+'env=do-ajax';

			// interval counter
			if( t.attr('interval') )
				href+= '&interval_counter=' + $(this).attr('interval_counter');

			// prompt
			if( t.attr('prompt') ){
				pmt = t.attr('prompt');
				if( msg = prompt(pmt) ){
					if( href.indexOf('{prompt}') == -1 ){
						href = href + msg;
					} else {
						href = href.replace("{prompt}", msg);
					}
				} else {
					return;
				}
			}

			// hide=1 - after passing confirm and prompt
			if( t.attr('hide') == 1 )
				if( t.attr('prompt') || t.attr('confirm') )
					t.hide();

			if( tagName == 'SELECT' )
				href+= (href.indexOf('?') == -1 ? '?' : '&') + t.attr('name') + '=' + t.val();


			// console.log(href);

			// the request
			$.ajax({ url: href, async: false }).done(function( html ) {

				// console.log(html);

				resp = doajax_resp_manipulate(html);

				if( resp.rel )
					t = $('[da-rel="'+resp.rel+'"]');

				if( resp.status == 'OK' ){
					
					// disload semaphore
					if( t.attr('disload') == 1 ){
						// t.toggleClass('disabled');
						t.removeAttr('disabled');
					}

					// hide someone else .re/input[type=submit]
					if( t.attr('hide') && t.attr('hide') != 1 ){
						
						hide = t.attr('hide');

						if( hide.indexOf('/') != -1 ){
							hide = hide.split('/');
							t.closest( hide[0] ).find( hide[1] ).hide();
						
						} else if( hide.substr(0,8) == 'closest(' ){
							hide = hide.substring(8, hide.length - 1);
							t.closest(hide).hide();

						} else {
							$(hide).hide();							
						}
					}

					// .re/input[type=submit]:btn-primary,btn-danger
					if( toggle_class = t.attr('toggle-class') ){
						if( toggle_class.indexOf(':') != -1 ){
							toggle_colon = toggle_class.split(':');
							toggle_class = toggle_colon[1];
							toggle_colon = toggle_colon[0];
							toggle_colon = toggle_colon.split('/');
							toggle_colon = t.closest( toggle_colon[0] );
							if( toggle_colon[1] ){
								toggle_colon = toggle_colon.find( toggle_colon[1] );
							}

						} else {
							toggle_colon = t;
						}
						//
						tc_arr = toggle_class.split(',');
						for( i=0; i< tc_arr.length; i++ ){
							toggle_colon.toggleClass( tc_arr[i] );
						}
					}

					// .n1/.PIU
					if( toggleDisabled = t.attr('toggle') ){
						if( toggleDisabled == 1 ){
							toggleDisabled = t;
						} else {
							toggleDisabled = toggleDisabled.split('/');
							toggleDisabled = t.closest( toggleDisabled[0] ).find( toggleDisabled[1] );
						}
						if (toggleDisabled.attr('disabled')){
							toggleDisabled.removeAttr('disabled');
						} else {
							toggleDisabled.attr('disabled', 'disabled');
						}
					}

					// alert
					if( resp.message ){
						if(! resp.messageDelay ){
							resp.messageDelay = 1
						}
						alert(resp.message, resp.messageDelay);
					}
					
					// eval
					if( resp.eval_code )
						$.globalEval(resp.eval_code);

					// in
					if(typeof resp.text !== 'undefined') {
						if( the_in = t.attr('in') ){
							if( the_in == 'me' ){
								t_in = t;
							} else if( the_in.indexOf('/') != -1 ){
								in_arr = the_in.split('/');
								t_in = t.closest( in_arr[0] ).find( in_arr[1] );
							} else {
								t_in = $(the_in);
							}
							if( t_in.is("input") || t_in.is("select") || t_in.is("textarea") ){
								t_in.val(resp.text);
							} else {
								t_in.html(resp.text);
							}
						}
					}

				} else {
					// 
				}
				
			});

			e.preventDefault();
			return;

		}
		// end href
		
	}).on('keydown', function(e){

		// vars
		t = $(this);

		// onsave
		if( onsave = t.attr('onsave') ){
			
			// alert(e.keyCode + '- ' + e.key + ' -- ' + e.code );
			
			// if( e.key == 'š' ){
			// 	alert('yes');
			// }

			// console.log( t.prop("tagName") );

			if( ( ( e.ctrlKey || e.metaKey ) && (e.keyCode == 83) ) || ( e.key == 'š' ) || ( t.prop("tagName")=='INPUT' && (e.keyCode == 13) ) ){
				
				var obj = {};
			    obj[ t.attr('name') ] = t.val();
				$.ajax({url:onsave,type:'POST',data:obj}).done(function(r){
					if(r=='OK'){
						alert('Saved.',0.5);
					} else {
						console.log(r);
					}
				});

				e.preventDefault();
				return;
				
			}
		}
		// end onsave

	});


	$(".do-ajax > *,[do-ajax] > *").on('click', function(e) {
	    // e.stopPropagation();
	    return;
	});


	// file upload
	// hide the element, and trigger on 'i[0]' part one
	// if any i[1] then put the uploaded link on it.
	$('input.do-ajax[type=file],input[do-ajax][type=file]').each(function(index, el) {

		t = $(this);

		if( i = t.attr('i') ){
			
			if(i.indexOf('/')){
				[i, j] = i.split('/');
			}
			if( i == '' ){
				i = t;
				t.show();

			} else {
				
				t.after('<i class="do-ajax-i '+i+'" ></i>');
				i = t.next('i');

				i.on('click', function(e){
					t = $(this).prev('input[type=file]');
					t.trigger('click');
				});
			}
			
			if(j){
				i.after('<a class="do-ajax-j" target=_blank href=""><i class="'+j+'" ></i></a>');
				j = i.next('a');
				if( def = t.attr('default') ){
					j.attr('href', def);
				} else {
					j.hide();
				}
			}

		} else {
			t.show();
		}

	});

	// file upload
	$('input.do-ajax[type=file],input[do-ajax][type=file]').on('change', function(e){

		t = $(this);

		i = t.next();
		j = i.next();
		n = t.attr('name');

		// multiple trigger
		if( t.attr('do-ajax') ){
			f = t.attr('do-ajax');
		} else if( t.attr('feed') ){
			f = t.attr('feed');
		} else if( t.attr('href') ){
			f = t.attr('href');
		} else {
			return;
		}

		if( t.val() ){
			if(j){
				j.show().css({'opacity':0.5});
			}
			var file_data = t.prop('files')[0];
		    var form_data = new FormData();
		    form_data.append(n, file_data);
		    $.ajax({
		        url: f,
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: form_data,
		        type: 'post',
		        success: function(c){
		        	if( isJson(c) ){
		        		c = $.parseJSON(c);
		        		if( c.status=='OK' ){
		        			alert('File uploaded.', 0.5);
			        		if(j){
			        			j.attr('href', c.url).css({'opacity':1});
			        		}
			        	}
		        	}
		        	t.val('');
		        }
		     });
		}

	});


	//
	// interval
	function funInterval(elem, timer){
		
		// run it first,
	    elem.trigger('click');

	    // and repeat it
		timer*= 1000;
		setInterval(function() {
			elem.attr('interval_counter', parseInt(elem.attr('interval_counter')) + 1 );
		    elem.trigger('click');  
		}, timer);

	}
	//
	$('.do-ajax[interval],[do-ajax][interval]').each(function(i, html){
		$(this).attr('interval_counter', '0');
		funInterval( $(this), $(this).attr('interval') );
	});
	//



});




function isJson(str) {
    
    try {
        json = JSON.parse(str);
    
    } catch (e) {
        return false;
    }
    
    return json;

}


function doajax_resp_manipulate( html ){

	if( json = isJson(html) ){
    	return json;

    } else {

		// pattern of return in do-ajax
		// OK --> 2-character status
		// theInsertText --> text for 'in' section
		// #eval --> eval command
		// alert('Hi');
		// #alert --> alert sign
		// Saved. --> alert message

    	resp = {};

		if( html.substring(0,2) == 'OK' ){
			resp.status = 'OK';
		}

		text = html.substring(2, html.length);

		if( text.indexOf('#alert') != -1 ){
			[ text, message ] = text.split('#alert');
			resp.message = message;
		}
		
		if( text.indexOf('#eval') != -1 ){
			[ text, eval_code ] = text.split('#eval');
			resp.eval_code = eval_code
		}

		resp.text = text;

		return resp;

	}

}




!function(n){"use strict";function t(n,t){var r=(65535&n)+(65535&t),e=(n>>16)+(t>>16)+(r>>16);return e<<16|65535&r}function r(n,t){return n<<t|n>>>32-t}function e(n,e,o,u,c,f){return t(r(t(t(e,n),t(u,f)),c),o)}function o(n,t,r,o,u,c,f){return e(t&r|~t&o,n,t,u,c,f)}function u(n,t,r,o,u,c,f){return e(t&o|r&~o,n,t,u,c,f)}function c(n,t,r,o,u,c,f){return e(t^r^o,n,t,u,c,f)}function f(n,t,r,o,u,c,f){return e(r^(t|~o),n,t,u,c,f)}function i(n,r){n[r>>5]|=128<<r%32,n[(r+64>>>9<<4)+14]=r;var e,i,a,h,d,l=1732584193,g=-271733879,v=-1732584194,m=271733878;for(e=0;e<n.length;e+=16)i=l,a=g,h=v,d=m,l=o(l,g,v,m,n[e],7,-680876936),m=o(m,l,g,v,n[e+1],12,-389564586),v=o(v,m,l,g,n[e+2],17,606105819),g=o(g,v,m,l,n[e+3],22,-1044525330),l=o(l,g,v,m,n[e+4],7,-176418897),m=o(m,l,g,v,n[e+5],12,1200080426),v=o(v,m,l,g,n[e+6],17,-1473231341),g=o(g,v,m,l,n[e+7],22,-45705983),l=o(l,g,v,m,n[e+8],7,1770035416),m=o(m,l,g,v,n[e+9],12,-1958414417),v=o(v,m,l,g,n[e+10],17,-42063),g=o(g,v,m,l,n[e+11],22,-1990404162),l=o(l,g,v,m,n[e+12],7,1804603682),m=o(m,l,g,v,n[e+13],12,-40341101),v=o(v,m,l,g,n[e+14],17,-1502002290),g=o(g,v,m,l,n[e+15],22,1236535329),l=u(l,g,v,m,n[e+1],5,-165796510),m=u(m,l,g,v,n[e+6],9,-1069501632),v=u(v,m,l,g,n[e+11],14,643717713),g=u(g,v,m,l,n[e],20,-373897302),l=u(l,g,v,m,n[e+5],5,-701558691),m=u(m,l,g,v,n[e+10],9,38016083),v=u(v,m,l,g,n[e+15],14,-660478335),g=u(g,v,m,l,n[e+4],20,-405537848),l=u(l,g,v,m,n[e+9],5,568446438),m=u(m,l,g,v,n[e+14],9,-1019803690),v=u(v,m,l,g,n[e+3],14,-187363961),g=u(g,v,m,l,n[e+8],20,1163531501),l=u(l,g,v,m,n[e+13],5,-1444681467),m=u(m,l,g,v,n[e+2],9,-51403784),v=u(v,m,l,g,n[e+7],14,1735328473),g=u(g,v,m,l,n[e+12],20,-1926607734),l=c(l,g,v,m,n[e+5],4,-378558),m=c(m,l,g,v,n[e+8],11,-2022574463),v=c(v,m,l,g,n[e+11],16,1839030562),g=c(g,v,m,l,n[e+14],23,-35309556),l=c(l,g,v,m,n[e+1],4,-1530992060),m=c(m,l,g,v,n[e+4],11,1272893353),v=c(v,m,l,g,n[e+7],16,-155497632),g=c(g,v,m,l,n[e+10],23,-1094730640),l=c(l,g,v,m,n[e+13],4,681279174),m=c(m,l,g,v,n[e],11,-358537222),v=c(v,m,l,g,n[e+3],16,-722521979),g=c(g,v,m,l,n[e+6],23,76029189),l=c(l,g,v,m,n[e+9],4,-640364487),m=c(m,l,g,v,n[e+12],11,-421815835),v=c(v,m,l,g,n[e+15],16,530742520),g=c(g,v,m,l,n[e+2],23,-995338651),l=f(l,g,v,m,n[e],6,-198630844),m=f(m,l,g,v,n[e+7],10,1126891415),v=f(v,m,l,g,n[e+14],15,-1416354905),g=f(g,v,m,l,n[e+5],21,-57434055),l=f(l,g,v,m,n[e+12],6,1700485571),m=f(m,l,g,v,n[e+3],10,-1894986606),v=f(v,m,l,g,n[e+10],15,-1051523),g=f(g,v,m,l,n[e+1],21,-2054922799),l=f(l,g,v,m,n[e+8],6,1873313359),m=f(m,l,g,v,n[e+15],10,-30611744),v=f(v,m,l,g,n[e+6],15,-1560198380),g=f(g,v,m,l,n[e+13],21,1309151649),l=f(l,g,v,m,n[e+4],6,-145523070),m=f(m,l,g,v,n[e+11],10,-1120210379),v=f(v,m,l,g,n[e+2],15,718787259),g=f(g,v,m,l,n[e+9],21,-343485551),l=t(l,i),g=t(g,a),v=t(v,h),m=t(m,d);return[l,g,v,m]}function a(n){var t,r="",e=32*n.length;for(t=0;t<e;t+=8)r+=String.fromCharCode(n[t>>5]>>>t%32&255);return r}function h(n){var t,r=[];for(r[(n.length>>2)-1]=void 0,t=0;t<r.length;t+=1)r[t]=0;var e=8*n.length;for(t=0;t<e;t+=8)r[t>>5]|=(255&n.charCodeAt(t/8))<<t%32;return r}function d(n){return a(i(h(n),8*n.length))}function l(n,t){var r,e,o=h(n),u=[],c=[];for(u[15]=c[15]=void 0,o.length>16&&(o=i(o,8*n.length)),r=0;r<16;r+=1)u[r]=909522486^o[r],c[r]=1549556828^o[r];return e=i(u.concat(h(t)),512+8*t.length),a(i(c.concat(e),640))}function g(n){var t,r,e="0123456789abcdef",o="";for(r=0;r<n.length;r+=1)t=n.charCodeAt(r),o+=e.charAt(t>>>4&15)+e.charAt(15&t);return o}function v(n){return unescape(encodeURIComponent(n))}function m(n){return d(v(n))}function p(n){return g(m(n))}function s(n,t){return l(v(n),v(t))}function C(n,t){return g(s(n,t))}function A(n,t,r){return t?r?s(t,n):C(t,n):r?m(n):p(n)}"function"==typeof define&&define.amd?define(function(){return A}):"object"==typeof module&&module.exports?module.exports=A:n.md5=A}(this);
//# sourceMappingURL=md5.min.js.map


